package com.taocares.sfss.dto;

import io.swagger.annotations.ApiModel;
import org.springframework.core.io.Resource;

@ApiModel("文件信息和文件流")
public class FileResourceDTO extends FileDTO {

	private Resource resource;

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof FileResourceDTO)) {
			return false;
		}
		FileResourceDTO other = (FileResourceDTO)o;
		if (!other.canEqual(this)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		Object this$resource = getResource();
		Object other$resource = other.getResource();
		return this$resource != null ? this$resource.equals(other$resource) : other$resource == null;
	}

	protected boolean canEqual(Object other) {
		return other instanceof FileResourceDTO;
	}

	public int hashCode() {
		int PRIME = 59;
		int result = super.hashCode();
		Object $resource = getResource();
		result = result * 59 + ($resource != null ? $resource.hashCode() : 43);
		return result;
	}

	public FileResourceDTO() {
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public String toString() {
		return (new StringBuilder()).append("FileResourceDTO(resource=").append(getResource()).append(")").toString();
	}
}
