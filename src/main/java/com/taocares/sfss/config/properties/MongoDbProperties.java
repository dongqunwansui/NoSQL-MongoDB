// Decompiled by Jad v1.5.8e2. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://kpdus.tripod.com/jad.html
// Decompiler options: packimports(3) braces fieldsfirst ansi nonlb space 
// Source File Name:   MongoDbProperties.java

package com.taocares.sfss.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import java.util.List;
@Valid
@Component
@ConfigurationProperties("mongodb")
public class MongoDbProperties {

	private String database;
	private List address;
	private String replicaSet;
	private String username;
	private String password;
	private Integer connectionsPerHost;
	private Integer minConnectionsPerHost;
	private Integer threadsAllowedToBlockForConnectionMultiplier;
	private Integer serverSelectionTimeout;
	private Integer maxWaitTime;
	private Integer maxConnectionIdleTime;
	private Integer maxConnectionLifeTime;
	private Integer connectTimeout;
	private Integer socketTimeout;
	private Boolean socketKeepAlive;
	private Boolean sslEnabled;
	private Boolean sslInvalidHostNameAllowed;
	private Boolean alwaysUseMBeans;
	private Integer heartbeatFrequency;
	private Integer minHeartbeatFrequency;
	private Integer heartbeatConnectTimeout;
	private Integer heartbeatSocketTimeout;
	private Integer localThreshold;
	private String authenticationDatabase;

	public MongoDbProperties() {
		connectionsPerHost = Integer.valueOf(50);
		minConnectionsPerHost = Integer.valueOf(1);
		threadsAllowedToBlockForConnectionMultiplier = Integer.valueOf(5);
		serverSelectionTimeout = Integer.valueOf(30000);
		maxWaitTime = Integer.valueOf(0x1d4c0);
		maxConnectionIdleTime = Integer.valueOf(0);
		maxConnectionLifeTime = Integer.valueOf(0);
		connectTimeout = Integer.valueOf(10000);
		socketTimeout = Integer.valueOf(0);
		socketKeepAlive = Boolean.valueOf(false);
		sslEnabled = Boolean.valueOf(false);
		sslInvalidHostNameAllowed = Boolean.valueOf(false);
		alwaysUseMBeans = Boolean.valueOf(false);
		heartbeatFrequency = Integer.valueOf(10000);
		minHeartbeatFrequency = Integer.valueOf(500);
		heartbeatConnectTimeout = Integer.valueOf(20000);
		heartbeatSocketTimeout = Integer.valueOf(20000);
		localThreshold = Integer.valueOf(15);
	}

	public String getDatabase() {
		return database;
	}

	public List getAddress() {
		return address;
	}

	public String getReplicaSet() {
		return replicaSet;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public Integer getConnectionsPerHost() {
		return connectionsPerHost;
	}

	public Integer getMinConnectionsPerHost() {
		return minConnectionsPerHost;
	}

	public Integer getThreadsAllowedToBlockForConnectionMultiplier() {
		return threadsAllowedToBlockForConnectionMultiplier;
	}

	public Integer getServerSelectionTimeout() {
		return serverSelectionTimeout;
	}

	public Integer getMaxWaitTime() {
		return maxWaitTime;
	}

	public Integer getMaxConnectionIdleTime() {
		return maxConnectionIdleTime;
	}

	public Integer getMaxConnectionLifeTime() {
		return maxConnectionLifeTime;
	}

	public Integer getConnectTimeout() {
		return connectTimeout;
	}

	public Integer getSocketTimeout() {
		return socketTimeout;
	}

	public Boolean getSocketKeepAlive() {
		return socketKeepAlive;
	}

	public Boolean getSslEnabled() {
		return sslEnabled;
	}

	public Boolean getSslInvalidHostNameAllowed() {
		return sslInvalidHostNameAllowed;
	}

	public Boolean getAlwaysUseMBeans() {
		return alwaysUseMBeans;
	}

	public Integer getHeartbeatFrequency() {
		return heartbeatFrequency;
	}

	public Integer getMinHeartbeatFrequency() {
		return minHeartbeatFrequency;
	}

	public Integer getHeartbeatConnectTimeout() {
		return heartbeatConnectTimeout;
	}

	public Integer getHeartbeatSocketTimeout() {
		return heartbeatSocketTimeout;
	}

	public Integer getLocalThreshold() {
		return localThreshold;
	}

	public String getAuthenticationDatabase() {
		return authenticationDatabase;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public void setAddress(List address) {
		this.address = address;
	}

	public void setReplicaSet(String replicaSet) {
		this.replicaSet = replicaSet;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setConnectionsPerHost(Integer connectionsPerHost) {
		this.connectionsPerHost = connectionsPerHost;
	}

	public void setMinConnectionsPerHost(Integer minConnectionsPerHost) {
		this.minConnectionsPerHost = minConnectionsPerHost;
	}

	public void setThreadsAllowedToBlockForConnectionMultiplier(Integer threadsAllowedToBlockForConnectionMultiplier) {
		this.threadsAllowedToBlockForConnectionMultiplier = threadsAllowedToBlockForConnectionMultiplier;
	}

	public void setServerSelectionTimeout(Integer serverSelectionTimeout) {
		this.serverSelectionTimeout = serverSelectionTimeout;
	}

	public void setMaxWaitTime(Integer maxWaitTime) {
		this.maxWaitTime = maxWaitTime;
	}

	public void setMaxConnectionIdleTime(Integer maxConnectionIdleTime) {
		this.maxConnectionIdleTime = maxConnectionIdleTime;
	}

	public void setMaxConnectionLifeTime(Integer maxConnectionLifeTime) {
		this.maxConnectionLifeTime = maxConnectionLifeTime;
	}

	public void setConnectTimeout(Integer connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public void setSocketTimeout(Integer socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public void setSocketKeepAlive(Boolean socketKeepAlive) {
		this.socketKeepAlive = socketKeepAlive;
	}

	public void setSslEnabled(Boolean sslEnabled) {
		this.sslEnabled = sslEnabled;
	}

	public void setSslInvalidHostNameAllowed(Boolean sslInvalidHostNameAllowed) {
		this.sslInvalidHostNameAllowed = sslInvalidHostNameAllowed;
	}

	public void setAlwaysUseMBeans(Boolean alwaysUseMBeans) {
		this.alwaysUseMBeans = alwaysUseMBeans;
	}

	public void setHeartbeatFrequency(Integer heartbeatFrequency) {
		this.heartbeatFrequency = heartbeatFrequency;
	}

	public void setMinHeartbeatFrequency(Integer minHeartbeatFrequency) {
		this.minHeartbeatFrequency = minHeartbeatFrequency;
	}

	public void setHeartbeatConnectTimeout(Integer heartbeatConnectTimeout) {
		this.heartbeatConnectTimeout = heartbeatConnectTimeout;
	}

	public void setHeartbeatSocketTimeout(Integer heartbeatSocketTimeout) {
		this.heartbeatSocketTimeout = heartbeatSocketTimeout;
	}

	public void setLocalThreshold(Integer localThreshold) {
		this.localThreshold = localThreshold;
	}

	public void setAuthenticationDatabase(String authenticationDatabase) {
		this.authenticationDatabase = authenticationDatabase;
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof MongoDbProperties)) {
			return false;
		}
		MongoDbProperties other = (MongoDbProperties)o;
		if (!other.canEqual(this)) {
			return false;
		}
		Object this$database = getDatabase();
		Object other$database = other.getDatabase();
		if (this$database != null ? !this$database.equals(other$database) : other$database != null) {
			return false;
		}
		Object this$address = getAddress();
		Object other$address = other.getAddress();
		if (this$address != null ? !this$address.equals(other$address) : other$address != null) {
			return false;
		}
		Object this$replicaSet = getReplicaSet();
		Object other$replicaSet = other.getReplicaSet();
		if (this$replicaSet != null ? !this$replicaSet.equals(other$replicaSet) : other$replicaSet != null) {
			return false;
		}
		Object this$username = getUsername();
		Object other$username = other.getUsername();
		if (this$username != null ? !this$username.equals(other$username) : other$username != null) {
			return false;
		}
		Object this$password = getPassword();
		Object other$password = other.getPassword();
		if (this$password != null ? !this$password.equals(other$password) : other$password != null) {
			return false;
		}
		Object this$connectionsPerHost = getConnectionsPerHost();
		Object other$connectionsPerHost = other.getConnectionsPerHost();
		if (this$connectionsPerHost != null ? !this$connectionsPerHost.equals(other$connectionsPerHost) : other$connectionsPerHost != null) {
			return false;
		}
		Object this$minConnectionsPerHost = getMinConnectionsPerHost();
		Object other$minConnectionsPerHost = other.getMinConnectionsPerHost();
		if (this$minConnectionsPerHost != null ? !this$minConnectionsPerHost.equals(other$minConnectionsPerHost) : other$minConnectionsPerHost != null) {
			return false;
		}
		Object this$threadsAllowedToBlockForConnectionMultiplier = getThreadsAllowedToBlockForConnectionMultiplier();
		Object other$threadsAllowedToBlockForConnectionMultiplier = other.getThreadsAllowedToBlockForConnectionMultiplier();
		if (this$threadsAllowedToBlockForConnectionMultiplier != null ? !this$threadsAllowedToBlockForConnectionMultiplier.equals(other$threadsAllowedToBlockForConnectionMultiplier) : other$threadsAllowedToBlockForConnectionMultiplier != null) {
			return false;
		}
		Object this$serverSelectionTimeout = getServerSelectionTimeout();
		Object other$serverSelectionTimeout = other.getServerSelectionTimeout();
		if (this$serverSelectionTimeout != null ? !this$serverSelectionTimeout.equals(other$serverSelectionTimeout) : other$serverSelectionTimeout != null) {
			return false;
		}
		Object this$maxWaitTime = getMaxWaitTime();
		Object other$maxWaitTime = other.getMaxWaitTime();
		if (this$maxWaitTime != null ? !this$maxWaitTime.equals(other$maxWaitTime) : other$maxWaitTime != null) {
			return false;
		}
		Object this$maxConnectionIdleTime = getMaxConnectionIdleTime();
		Object other$maxConnectionIdleTime = other.getMaxConnectionIdleTime();
		if (this$maxConnectionIdleTime != null ? !this$maxConnectionIdleTime.equals(other$maxConnectionIdleTime) : other$maxConnectionIdleTime != null) {
			return false;
		}
		Object this$maxConnectionLifeTime = getMaxConnectionLifeTime();
		Object other$maxConnectionLifeTime = other.getMaxConnectionLifeTime();
		if (this$maxConnectionLifeTime != null ? !this$maxConnectionLifeTime.equals(other$maxConnectionLifeTime) : other$maxConnectionLifeTime != null) {
			return false;
		}
		Object this$connectTimeout = getConnectTimeout();
		Object other$connectTimeout = other.getConnectTimeout();
		if (this$connectTimeout != null ? !this$connectTimeout.equals(other$connectTimeout) : other$connectTimeout != null) {
			return false;
		}
		Object this$socketTimeout = getSocketTimeout();
		Object other$socketTimeout = other.getSocketTimeout();
		if (this$socketTimeout != null ? !this$socketTimeout.equals(other$socketTimeout) : other$socketTimeout != null) {
			return false;
		}
		Object this$socketKeepAlive = getSocketKeepAlive();
		Object other$socketKeepAlive = other.getSocketKeepAlive();
		if (this$socketKeepAlive != null ? !this$socketKeepAlive.equals(other$socketKeepAlive) : other$socketKeepAlive != null) {
			return false;
		}
		Object this$sslEnabled = getSslEnabled();
		Object other$sslEnabled = other.getSslEnabled();
		if (this$sslEnabled != null ? !this$sslEnabled.equals(other$sslEnabled) : other$sslEnabled != null) {
			return false;
		}
		Object this$sslInvalidHostNameAllowed = getSslInvalidHostNameAllowed();
		Object other$sslInvalidHostNameAllowed = other.getSslInvalidHostNameAllowed();
		if (this$sslInvalidHostNameAllowed != null ? !this$sslInvalidHostNameAllowed.equals(other$sslInvalidHostNameAllowed) : other$sslInvalidHostNameAllowed != null) {
			return false;
		}
		Object this$alwaysUseMBeans = getAlwaysUseMBeans();
		Object other$alwaysUseMBeans = other.getAlwaysUseMBeans();
		if (this$alwaysUseMBeans != null ? !this$alwaysUseMBeans.equals(other$alwaysUseMBeans) : other$alwaysUseMBeans != null) {
			return false;
		}
		Object this$heartbeatFrequency = getHeartbeatFrequency();
		Object other$heartbeatFrequency = other.getHeartbeatFrequency();
		if (this$heartbeatFrequency != null ? !this$heartbeatFrequency.equals(other$heartbeatFrequency) : other$heartbeatFrequency != null) {
			return false;
		}
		Object this$minHeartbeatFrequency = getMinHeartbeatFrequency();
		Object other$minHeartbeatFrequency = other.getMinHeartbeatFrequency();
		if (this$minHeartbeatFrequency != null ? !this$minHeartbeatFrequency.equals(other$minHeartbeatFrequency) : other$minHeartbeatFrequency != null) {
			return false;
		}
		Object this$heartbeatConnectTimeout = getHeartbeatConnectTimeout();
		Object other$heartbeatConnectTimeout = other.getHeartbeatConnectTimeout();
		if (this$heartbeatConnectTimeout != null ? !this$heartbeatConnectTimeout.equals(other$heartbeatConnectTimeout) : other$heartbeatConnectTimeout != null) {
			return false;
		}
		Object this$heartbeatSocketTimeout = getHeartbeatSocketTimeout();
		Object other$heartbeatSocketTimeout = other.getHeartbeatSocketTimeout();
		if (this$heartbeatSocketTimeout != null ? !this$heartbeatSocketTimeout.equals(other$heartbeatSocketTimeout) : other$heartbeatSocketTimeout != null) {
			return false;
		}
		Object this$localThreshold = getLocalThreshold();
		Object other$localThreshold = other.getLocalThreshold();
		if (this$localThreshold != null ? !this$localThreshold.equals(other$localThreshold) : other$localThreshold != null) {
			return false;
		}
		Object this$authenticationDatabase = getAuthenticationDatabase();
		Object other$authenticationDatabase = other.getAuthenticationDatabase();
		return this$authenticationDatabase != null ? this$authenticationDatabase.equals(other$authenticationDatabase) : other$authenticationDatabase == null;
	}

	protected boolean canEqual(Object other) {
		return other instanceof MongoDbProperties;
	}

	public int hashCode() {
		int PRIME = 59;
		int result = 1;
		Object $database = getDatabase();
		result = result * 59 + ($database != null ? $database.hashCode() : 43);
		Object $address = getAddress();
		result = result * 59 + ($address != null ? $address.hashCode() : 43);
		Object $replicaSet = getReplicaSet();
		result = result * 59 + ($replicaSet != null ? $replicaSet.hashCode() : 43);
		Object $username = getUsername();
		result = result * 59 + ($username != null ? $username.hashCode() : 43);
		Object $password = getPassword();
		result = result * 59 + ($password != null ? $password.hashCode() : 43);
		Object $connectionsPerHost = getConnectionsPerHost();
		result = result * 59 + ($connectionsPerHost != null ? $connectionsPerHost.hashCode() : 43);
		Object $minConnectionsPerHost = getMinConnectionsPerHost();
		result = result * 59 + ($minConnectionsPerHost != null ? $minConnectionsPerHost.hashCode() : 43);
		Object $threadsAllowedToBlockForConnectionMultiplier = getThreadsAllowedToBlockForConnectionMultiplier();
		result = result * 59 + ($threadsAllowedToBlockForConnectionMultiplier != null ? $threadsAllowedToBlockForConnectionMultiplier.hashCode() : 43);
		Object $serverSelectionTimeout = getServerSelectionTimeout();
		result = result * 59 + ($serverSelectionTimeout != null ? $serverSelectionTimeout.hashCode() : 43);
		Object $maxWaitTime = getMaxWaitTime();
		result = result * 59 + ($maxWaitTime != null ? $maxWaitTime.hashCode() : 43);
		Object $maxConnectionIdleTime = getMaxConnectionIdleTime();
		result = result * 59 + ($maxConnectionIdleTime != null ? $maxConnectionIdleTime.hashCode() : 43);
		Object $maxConnectionLifeTime = getMaxConnectionLifeTime();
		result = result * 59 + ($maxConnectionLifeTime != null ? $maxConnectionLifeTime.hashCode() : 43);
		Object $connectTimeout = getConnectTimeout();
		result = result * 59 + ($connectTimeout != null ? $connectTimeout.hashCode() : 43);
		Object $socketTimeout = getSocketTimeout();
		result = result * 59 + ($socketTimeout != null ? $socketTimeout.hashCode() : 43);
		Object $socketKeepAlive = getSocketKeepAlive();
		result = result * 59 + ($socketKeepAlive != null ? $socketKeepAlive.hashCode() : 43);
		Object $sslEnabled = getSslEnabled();
		result = result * 59 + ($sslEnabled != null ? $sslEnabled.hashCode() : 43);
		Object $sslInvalidHostNameAllowed = getSslInvalidHostNameAllowed();
		result = result * 59 + ($sslInvalidHostNameAllowed != null ? $sslInvalidHostNameAllowed.hashCode() : 43);
		Object $alwaysUseMBeans = getAlwaysUseMBeans();
		result = result * 59 + ($alwaysUseMBeans != null ? $alwaysUseMBeans.hashCode() : 43);
		Object $heartbeatFrequency = getHeartbeatFrequency();
		result = result * 59 + ($heartbeatFrequency != null ? $heartbeatFrequency.hashCode() : 43);
		Object $minHeartbeatFrequency = getMinHeartbeatFrequency();
		result = result * 59 + ($minHeartbeatFrequency != null ? $minHeartbeatFrequency.hashCode() : 43);
		Object $heartbeatConnectTimeout = getHeartbeatConnectTimeout();
		result = result * 59 + ($heartbeatConnectTimeout != null ? $heartbeatConnectTimeout.hashCode() : 43);
		Object $heartbeatSocketTimeout = getHeartbeatSocketTimeout();
		result = result * 59 + ($heartbeatSocketTimeout != null ? $heartbeatSocketTimeout.hashCode() : 43);
		Object $localThreshold = getLocalThreshold();
		result = result * 59 + ($localThreshold != null ? $localThreshold.hashCode() : 43);
		Object $authenticationDatabase = getAuthenticationDatabase();
		result = result * 59 + ($authenticationDatabase != null ? $authenticationDatabase.hashCode() : 43);
		return result;
	}

	public String toString() {
		return (new StringBuilder()).append("MongoDbProperties(database=").append(getDatabase()).append(", address=").append(getAddress()).append(", replicaSet=").append(getReplicaSet()).append(", username=").append(getUsername()).append(", password=").append(getPassword()).append(", connectionsPerHost=").append(getConnectionsPerHost()).append(", minConnectionsPerHost=").append(getMinConnectionsPerHost()).append(", threadsAllowedToBlockForConnectionMultiplier=").append(getThreadsAllowedToBlockForConnectionMultiplier()).append(", serverSelectionTimeout=").append(getServerSelectionTimeout()).append(", maxWaitTime=").append(getMaxWaitTime()).append(", maxConnectionIdleTime=").append(getMaxConnectionIdleTime()).append(", maxConnectionLifeTime=").append(getMaxConnectionLifeTime()).append(", connectTimeout=").append(getConnectTimeout()).append(", socketTimeout=").append(getSocketTimeout()).append(", socketKeepAlive=").append(getSocketKeepAlive()).append(", sslEnabled=").append(getSslEnabled()).append(", sslInvalidHostNameAllowed=").append(getSslInvalidHostNameAllowed()).append(", alwaysUseMBeans=").append(getAlwaysUseMBeans()).append(", heartbeatFrequency=").append(getHeartbeatFrequency()).append(", minHeartbeatFrequency=").append(getMinHeartbeatFrequency()).append(", heartbeatConnectTimeout=").append(getHeartbeatConnectTimeout()).append(", heartbeatSocketTimeout=").append(getHeartbeatSocketTimeout()).append(", localThreshold=").append(getLocalThreshold()).append(", authenticationDatabase=").append(getAuthenticationDatabase()).append(")").toString();
	}
}
