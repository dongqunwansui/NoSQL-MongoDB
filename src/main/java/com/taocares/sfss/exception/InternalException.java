package com.taocares.sfss.exception;


public class InternalException extends RuntimeException {

	public InternalException(String message) {
		super(message);
	}
}
