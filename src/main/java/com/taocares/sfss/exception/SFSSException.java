package com.taocares.sfss.exception;


public class SFSSException extends RuntimeException {

	private Integer status;
	private String code;

	public SFSSException(ExceptionType e, String message) {
		super(message);
		status = e.getStatus();
		code = e.getCode();
	}

	public Integer getStatus() {
		return status;
	}

	public String getCode() {
		return code;
	}



	public enum  ExceptionType{

		FILE_NOT_EXIST(400, "FileNotExist"),MISSING_REQUEST_BODY(400,"InvalidBody"),MISSING_PARAM_IN_FORM(400, "MissingParamInForm");

		private Integer status;
		private String code;



		public Integer getStatus() {
			return status;
		}

		public String getCode() {
			return code;
		}


		ExceptionType(Integer status, String code) {
			this.status = status;
			this.code = code;
		}
	}


}
