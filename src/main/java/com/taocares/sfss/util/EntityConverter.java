// Decompiled by Jad v1.5.8e2. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://kpdus.tripod.com/jad.html
// Decompiler options: packimports(3) braces fieldsfirst ansi nonlb space 
// Source File Name:   EntityConverter.java

package com.taocares.sfss.util;

import com.taocares.sfss.dto.FileDTO;
import com.taocares.sfss.dto.FileResourceDTO;
import com.taocares.sfss.entity.FileEntity;
import org.springframework.core.io.Resource;

public class EntityConverter {

	public EntityConverter() {
	}

	public static FileDTO toFileDTO(FileEntity entity) {
		FileDTO dto = new FileDTO();
		setValueForFileDTO(entity, dto);
		return dto;
	}

	public static FileResourceDTO toFileResourceDTO(FileEntity entity, Resource resource) {
		FileResourceDTO dto = new FileResourceDTO();
		setValueForFileDTO(entity, dto);
		dto.setResource(resource);
		return dto;
	}

	private static void setValueForFileDTO(FileEntity entity, FileDTO dto) {
		dto.setId(entity.getId());
		dto.setOriginalName(entity.getOriginalName());
		dto.setType(entity.getType());
		dto.setSize(entity.getSize());
		dto.setEtag(entity.getEtag());
		dto.setCreatedTime(entity.getCreatedTime());
	}
}
