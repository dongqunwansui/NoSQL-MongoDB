// Decompiled by Jad v1.5.8e2. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://kpdus.tripod.com/jad.html
// Decompiler options: packimports(3) braces fieldsfirst ansi nonlb space 
// Source File Name:   HashUtil.java

package com.taocares.sfss.util;

import com.twmacinta.util.MD5;
import com.twmacinta.util.MD5InputStream;
import java.io.*;

public class HashUtil {

	public HashUtil() {
	}

	public static String md5(InputStream stream) throws IOException {
		long buf_size = stream.available();
		if (buf_size < 512L) {
			buf_size = 512L;
		}
		if (buf_size > 0x10000L) {
			buf_size = 0x10000L;
		}
		byte buf[] = new byte[(int)buf_size];
		MD5InputStream in;
		for (in = new MD5InputStream(stream); in.read(buf) != -1;) { }
		in.close();
		return MD5.asHex(in.hash());
	}

	public static String md5(File file) throws IOException {
		return MD5.asHex(MD5.getHash(file));
	}
}
