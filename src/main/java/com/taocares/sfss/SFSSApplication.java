package com.taocares.sfss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class SFSSApplication {

	public static void main(String args[]) {
		SpringApplication.run(SFSSApplication.class, args);
	}
}
