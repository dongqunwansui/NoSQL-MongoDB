package com.taocares.sfss.aspect;

import javax.servlet.http.HttpServletRequest;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Aspect
@Component
public class RequestAspect {
	private static final Logger log = LoggerFactory.getLogger(RequestAspect.class);



	@Pointcut("execution(public * com.taocares.sfss.controller..*(..))")
	private void log() {}



	@Around("log()")
	public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
		long time;
		HttpServletRequest request;
		time = System.currentTimeMillis();
		ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		assert attributes != null;
		request = attributes.getRequest();
		try {
			return joinPoint.proceed();
		} finally {
			log.info("ip={} method={} url={} classMethod={} spend={}ms", new Object[] {
					request.getRemoteAddr(), request
					.getMethod(), request
					.getRequestURL(), joinPoint
					.getSignature().getDeclaringTypeName() + "::" + joinPoint.getSignature().getName(),
					Long.valueOf(System.currentTimeMillis() - time) });
		}
	}
}

