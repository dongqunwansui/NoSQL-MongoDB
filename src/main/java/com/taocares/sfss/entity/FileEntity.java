// Decompiled by Jad v1.5.8e2. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://kpdus.tripod.com/jad.html
// Decompiler options: packimports(3) braces fieldsfirst ansi nonlb space 
// Source File Name:   FileEntity.java

package com.taocares.sfss.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document("taoFile")
public class FileEntity {

	private String id;
	private String originalName;
	private String etag;
	private String type;
	private String fileId;
	private long size;
	private Date createdTime;

	public FileEntity() {
	}

	public String getId() {
		return id;
	}

	public String getOriginalName() {
		return originalName;
	}

	public String getEtag() {
		return etag;
	}

	public String getType() {
		return type;
	}

	public String getFileId() {
		return fileId;
	}

	public long getSize() {
		return size;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public void setEtag(String etag) {
		this.etag = etag;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof FileEntity)) {
			return false;
		}
		FileEntity other = (FileEntity)o;
		if (!other.canEqual(this)) {
			return false;
		}
		Object this$id = getId();
		Object other$id = other.getId();
		if (this$id != null ? !this$id.equals(other$id) : other$id != null) {
			return false;
		}
		Object this$originalName = getOriginalName();
		Object other$originalName = other.getOriginalName();
		if (this$originalName != null ? !this$originalName.equals(other$originalName) : other$originalName != null) {
			return false;
		}
		Object this$etag = getEtag();
		Object other$etag = other.getEtag();
		if (this$etag != null ? !this$etag.equals(other$etag) : other$etag != null) {
			return false;
		}
		Object this$type = getType();
		Object other$type = other.getType();
		if (this$type != null ? !this$type.equals(other$type) : other$type != null) {
			return false;
		}
		Object this$fileId = getFileId();
		Object other$fileId = other.getFileId();
		if (this$fileId != null ? !this$fileId.equals(other$fileId) : other$fileId != null) {
			return false;
		}
		if (getSize() != other.getSize()) {
			return false;
		}
		Object this$createdTime = getCreatedTime();
		Object other$createdTime = other.getCreatedTime();
		return this$createdTime != null ? this$createdTime.equals(other$createdTime) : other$createdTime == null;
	}

	protected boolean canEqual(Object other) {
		return other instanceof FileEntity;
	}

	public int hashCode() {
		int PRIME = 59;
		int result = 1;
		Object $id = getId();
		result = result * 59 + ($id != null ? $id.hashCode() : 43);
		Object $originalName = getOriginalName();
		result = result * 59 + ($originalName != null ? $originalName.hashCode() : 43);
		Object $etag = getEtag();
		result = result * 59 + ($etag != null ? $etag.hashCode() : 43);
		Object $type = getType();
		result = result * 59 + ($type != null ? $type.hashCode() : 43);
		Object $fileId = getFileId();
		result = result * 59 + ($fileId != null ? $fileId.hashCode() : 43);
		long $size = getSize();
		result = result * 59 + (int)($size >>> 32 ^ $size);
		Object $createdTime = getCreatedTime();
		result = result * 59 + ($createdTime != null ? $createdTime.hashCode() : 43);
		return result;
	}

	public String toString() {
		return (new StringBuilder()).append("FileEntity(id=").append(getId()).append(", originalName=").append(getOriginalName()).append(", etag=").append(getEtag()).append(", type=").append(getType()).append(", fileId=").append(getFileId()).append(", size=").append(getSize()).append(", createdTime=").append(getCreatedTime()).append(")").toString();
	}
}
